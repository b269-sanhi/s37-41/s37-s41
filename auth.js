// JSON web token or JWT is a way of securely passing information from the server to the frontend or to the other parts of server
const jwt = require("jsonwebtoken")

/*
ANATOMY
Header - type of toker and signing algorithm
Payload - contains the claims (id,email,isAdmin)
Signature - generate by putting the encoded header, the encoded payload and applying the algorith in the header
*/

const secret = "CourseBookingAPI"

// Token creation
module.exports.createAccessToken = (user) => {
	// payload
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}
	return jwt.sign(data,secret,{})
}


// Token verification

module.exports.verify = (req,res,next) => {
	let token = req.headers.authorization
	if (typeof token != "undefined"){
		console.log(token)
		token = token.slice(7, token.length)
		return jwt.verify(token, secret, (err,data) => {
			if (err){
				return res.send({auth: "failed"})
			}else {
				next()
			}
		})
	}else{
		return res.send({auth:"failed"})
	}
}

// Token decryption
module.exports.decode = (token) => {
	if (typeof token !=="undefined"){
		token = token.slice(7, token.length)
		return jwt.verify(token, secret, (err,data) => {
			if (err){
				return null
			}else {
				return jwt.decode(token,{complete:true}).payload
			}
		})
	}else{
		return null
	}
}
