const Course = require("../models/Course");
const bcrypt = require("bcrypt")
const auth = require("../auth")

module.exports.addCourse = (data) => {
	// User is an admin
	if (data.isAdmin) {
		// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
		// Uses the information from the request body to provide all the necessary information
		let newCourse = new Course({
			name : data.course.name,
			description : data.course.description,
			price : data.course.price
		});
		// Saves the created object to our database
		return newCourse.save().then((course, error) => {
			// Course creation successful
			if (error) {
				return false;
			// Course creation failed
			} else {
				return true;
			};
		});
	} 
	// User is not an admin
	let message = Promise.resolve ('User must be ADMIN to access this!')
	return message.then((value) => {{
		return {value}
	}})
};

// Retrieve ALL courses 
module.exports.getAllCourses = () =>{
	return Course.find({}).then(result =>{
		return result
	})
}
// para hanapin yung specific thing na gustong hanapin
// Retrive ACTIVE courses
module.exports.getAllActive = () =>{
	return Course.find({isActive:true}).then(result =>{
		return result
	})
}

// Retrive specific courses
module.exports.getCourse = (reqParams) =>{
	return Course.findById(reqParams.courseId).then(result =>{
		return result
	})
}

// Update a course

module.exports.updateCourse = (reqParams, reqBody) => {
	let updatedCourse = {
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	}
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course,error) => {
		if(error){
			return false
		}else {
			return true
		}
	})
}


// change status
module.exports.updateArchive = (reqParams, reqBody) => { 	
	let updatedArchive = {
		
		isActive: reqBody.isActive
		// isActive: false
	}
	return Course.findByIdAndUpdate(reqParams.courseId, updatedArchive).then((course, err) => { 		
		if (err){ 
			console.log(err)		
		return false; 	
	} else
		 { 			return true
		} 	
	})
	 }

